const OverwatchAPI = require('overwatch-api');
const Discord = require('discord.io');
const Auth = require('./auth.json');
// Configure logger settings
const logger = require('winston');
logger.remove(logger.transports.Console);
logger.add(new logger.transports.Console, {
    colorize: true
});
logger.level = 'debug';
// create bot
const bot = new Discord.Client({
    token: Auth.token,
    autorun: true,
});

let users = 

bot.on('ready', (evt) => {
});

bot.on('message', function (user, userID, channelID, message, evt) {
    // Our bot needs to know if it will execute a command
    // It will listen for messages that will start with `!`
    if (message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' ');
        var cmd = args[0];
       
        args = args.splice(1);
        switch(cmd) {
            // !ping
            case 'ping':
                bot.sendMessage({
                    to: channelID,
                    message: 'Pong!'
                });
            break;
            // Just add any case commands if you want to..
         }
     }
});

// bot.on('presence', (user, userId, status, game, event) => {
//     console.log('user: ', JSON.stringify(user));
//     console.log('userId: ', userId);
//     console.log('status: ', status);
//     console.log('game: ', JSON.stringify(game));
//     console.log('event: ', JSON.stringify(event));
//     const { nick, guild_id } = event.d;

//     // 
//     if (nick) {

//     }
// });

bot.on('guildMemberUpdate', (guildMember, member, event) => {
    console.log('guildMember: ', JSON.stringify(guildMember));
    console.log('?: ', JSON.stringify(member));
    console.log('event: ', event);
});

