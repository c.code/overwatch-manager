const TANK = [
                "D.Va",
                "Orisa",
                "Reinhardt",
                "Roadhog",
                "Winston",
                "Wrecking Ball",
                "Zarya"
             ];
const DAMAGE = [
                "Bastion",
                "Doomfist",
                "Genji",
                "Hanzo",
                "Junkrat",
                "McCree",
                "Mei",
                "Pharah",
                "Reaper",
                "Soldier: 76",
                "Sombra",
                "Symmetra",
                "Torbjorn",
                "Tracer",
                "Widowmaker"
               ];
const SUPPORT = [
                 "Ana",
                 "Brigitte",
                 "Lucio",
                 "Mercy",
                 "Moira",
                 "Zenyatta"
                ]


export function getRank(json)
{
    return json.profile.rank
}


export function getClass(json)
{
    var mostplayed = json.competitive.global.masteringHeroe
    if(TANK.includes(mostplayed))
    {
        return 'tank'
    }else if(DAMAGE.includes(mostplayed))
    {
        return 'damage'
    }else if(SUPPORT.includes(mostplayed))
    {
        return 'support'
    }
}

export function getTeams(users)
{
    var tanks = new Array()
    var damage = new Array()
    var support = new Array()

    var teams = new Array()

    for(var i = 0; i < users.length; i++)
    {
        switch(getClass(users[i]))
        {
            case 'tank':
                tanks.push(users[i])
                break
            case 'damage':
                damage.push(users[i])
                break
            case 'support':
                support.push(users[i])
                break
        }
    }

    tanks = sortBySR(tanks)
    damage = sortBySR(damage)
    support = sortBySR(support)

    // Finding the smallest array, the limiting factor
    var smallest = (tanks.length > damage.length) ? damage.length : tanks.length
    smallest = (smallest > support.length) ? support.length : smallest
    smallest = ((smallest % 2) === 0) ? smallest : smallest - 1

    // Making the teams
    for(var i = 0; i < smallest; i+=2)
    {
        var team = new Array()
        team.push(tanks[0])
        team.push(tanks[1])
        tank.splice(0, 2)

        team.push(damage[0])
        team.push(damage[1])
        damage.splice(0, 2)

        team.push(support[0])
        team.push(support[1])
        support.splice(0, 2)
        teams.push(team)
    }

    // The leftovers
    tanks.push.apply(tanks, damage.concat(support));

    teams.push(tanks)
    
    return teams
}

export function sortBySR(array)
{
    if(sortedBySR(array)) { return array }

    var pivindex = (array.length % 2) === 0 ? array.length / 2 : (array.length - 1) / 2
    var greaterarr = new Array()
    var lessarr = new Array()
    for(var i = 0; i < array.length; i++)
    {
        if(getRank(array[i]) > getRank(array[pivindex]))
        {
            greaterarr.unshift(array[i])
        }else
        {
            lessarr.unshift(array[i])
        }
    }

    return sortBySR(greaterarr).concat(sortBySR(lessarr))
}


export function sortedBySR(array)
{
    var grank = getRank(array[0])
    var sorted = true
    for(var i = 0; i < (array.length - 1); i++)
    {
        sorted = (array[i] >= array[i + 1])
    }
    return sorted
}